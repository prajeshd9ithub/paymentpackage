<?php

namespace Av_sistemi\Laravel_cartasi_payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartaSiController extends Controller
{
    public function index()
    {
        $message = "Laravel CartaSi Payment Package";
        return view('Laravel_cartasi_payment::payment', compact('message'));
    }

    public function checkout()
    {
        $url = "https://connect.icepay.com/api/v1/payment/checkout";
        $method = "POST";
        $merchant_id = "12345";
        $secret_code = "AbCdEfGhIjKlMnOpQrStUvWxYz1234567890AbCd";
        $details = '{"Timestamp":"2015-01-01T00:00:00"}';
        $checksum = hash('sha256',$url.$method.$merchant_id.$secret_code.$details);
        dd($checksum);
    }
}
