<?php

namespace Av_sistemi\Laravel_cartasi_payment;

use Illuminate\Support\ServiceProvider;

class CartaSiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
         $this->loadViewsFrom(__DIR__.'/views', 'Laravel_cartasi_payment');
         $this->publishes([
        __DIR__.'/views' => base_path('resources/views/av_sistemi/laravel_cartasi_payment'),
    ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Av_sistemi\Laravel_cartasi_payment\CartaSiController');
    }
}
